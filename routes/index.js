var express = require('express');
var router = express.Router();
var getParsed = require('../parser.js');

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log(getParsed('./input.xml') );
  res.render('index', { title: 'hello'});
});

module.exports = router;
