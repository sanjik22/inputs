var fs = require('fs'),
    xml2js = require('xml2js'),
    parser = new xml2js.Parser();

function getString(res) {
	console.log(res);
	var parameters = res['Parameters']['Parameter'],
		result = [];
	for (var i = 0; i < parameters.length; i++) {
		var element = {},
		current = parameters[i];
		element['current'] = parameters[i],
		element['id'] = current.Id[0],
		element['name'] = current.Name[0],
		element['value'] = current.Value[0],
		element['title'] = current.Description[0],
		element['type'] = current.Type[0];
		result.push(element);
	}
	return result;
}

getParsed = function(xml) {
	var f = fs.readFileSync(xml, 'utf8'),
		result;
	parser.parseString(f, function(err, res){
		result = getString(res);
	});
	return result;
} 

module.exports = getParsed;